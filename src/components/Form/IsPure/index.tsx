import React from 'react';
import {connect} from 'react-redux';
import {Field} from 'redux-form';
import cn from 'classnames';

import $l from 'lang';
import {SalaryType} from '../types';

interface RootState {
  form: {calcSalary: {
    values: {
      isPure: boolean,
      salaryType: SalaryType,
    },
  }},
}

const IsPureControl: React.FC<RootState> = (props) => {
  const {form: {calcSalary: {values}}} = props;
  const {isPure: value, salaryType} = values;
  const isHidden = salaryType === SalaryType.minWage;
  return (
    <label className={cn('toggle-control', isHidden && 'hidden')}>
      <span className={cn('label', !value && 'active')}>{$l.taxOptions[+false]}</span>
      <div className="custom-control custom-switch">
        <Field
          name="isPure"
          id="is-pure"
          component="input"
          type="checkbox"
          className="custom-control-input"
        />
        <label className="custom-control-label" htmlFor="is-pure" />{/* Bootstrap stuff */}
      </div>
      <span className={cn('label', value && 'active')}>{$l.taxOptions[+true]}</span>
    </label>
  );
};

const stateToProps = (state: RootState) => ({form: state.form});

export default connect(stateToProps)(IsPureControl);
