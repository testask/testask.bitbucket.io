import React from 'react';
import {connect} from 'react-redux';
import {Field} from 'redux-form';
import cn from 'classnames';

import $l, {numFormatter} from 'lang';
import {SalaryType} from '../types';
import Summary from './Summary';

interface RootState {
  form: {calcSalary: {
    values: {
      salaryType: SalaryType,
      isPure: boolean,
      sum: string,
    },
  }},
}

const getLabel = (type: string) => ({
  [`${SalaryType.daily}`]: $l.help.daily,
  [`${SalaryType.hourly}`]: $l.help.hourly,
}[type] || '');

const norm = (value: string): string => {
  if (!value) return value;
  const num = value.replace(/\D/g, '');
  return numFormatter.format(+num);
};

const SumControl: React.FC<RootState> = (props) => {
  const {form: {calcSalary: {values}}} = props;
  const {salaryType} = values;
  const isHidden = salaryType === SalaryType.minWage;
  return (
    <>
      <div className={cn('sum-control', isHidden && 'hidden')}>
        <Field
          name="sum"
          component="input"
          type="text"
          className="rounded-pill"
          normalize={norm}
        />
        <span className="label">{$l.currency + getLabel(salaryType)}</span>
      </div>
      {salaryType === SalaryType.monthly ? <Summary {...values} /> : null}
    </>
  );
};

const stateToProps = (state: RootState) => ({form: state.form});

export default connect(stateToProps)(SumControl);
