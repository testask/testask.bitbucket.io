import React from 'react';

import $l from 'lang';

interface Props {
  sum: string,
  isPure: boolean,
}

const evalSummary = (rawSum: string, isPure: boolean) => {
  const TAX = 0.13;
  const sum = +rawSum.replace(/\D/g, '');
  const total = isPure ? sum / (1 - TAX) : sum;
  const clear = isPure ? sum : total - TAX * sum;
  const taxes = total - clear;
  return [clear, taxes, total];
};

const Summary: React.FC<Props> = ({sum, isPure}) => {
  const items = evalSummary(sum, isPure);
  return (
    <div className="summary-wrap">
      {Object.values(items).map((value, index) => {
        const label = $l.summary[index];
        return (
          <div key={label} className="item">
            <strong>{$l.formatSum(+value)}</strong>
            <span>{label}</span>
          </div>
        );
      })}
    </div>
  );
};

export default Summary;
