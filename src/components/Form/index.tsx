import React from 'react';
import {reduxForm, InjectedFormProps} from 'redux-form';

import $l from 'lang';
import {SalaryType, Values} from './types';
import SalaryTypeControl from './SalaryType';
import IsPureControl from './IsPure';
import SumControl from './Sum';
import 'styles/form.scss';

const initialValues: Values = {
  sum: '40 000',
  salaryType: SalaryType.monthly,
  isPure: true,
};

type Props = InjectedFormProps<Values, Record<string, unknown>, string>;

const FormView: React.FC<Props> = ({handleSubmit}) => (
  <form onSubmit={handleSubmit} className="main-form">
    <h3 className="title">{$l.title}</h3>
    <SalaryTypeControl />
    <IsPureControl />
    <SumControl />
  </form>
);

const Form = reduxForm({
  form: 'calcSalary',
  initialValues,
})(FormView);

export default Form;
