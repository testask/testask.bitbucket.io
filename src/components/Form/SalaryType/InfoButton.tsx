import React, {useState} from 'react';
import cn from 'classnames';

interface Props {body: string}

const InfoButton: React.FC<Props> = ({body}) => {
  const [active, setActive] = useState(false);
  const [hover, setHover] = useState(false);
  const onClick = () => setActive((curr) => !curr);
  return (
    <span
      className="info-trigger"
      onClick={onClick}
      onMouseOver={() => setHover(true)}
      onMouseOut={() => setHover(false)}
    >
      {active ? '×' : 'i'}
      <div className={cn('popover', (active || hover) && 'active')}>
        {body}
      </div>
    </span>
  );
};

export default InfoButton;
