import React from 'react';
import {Field} from 'redux-form';

import $l from 'lang';
import {SalaryType} from '../types';
import Info from './InfoButton';

const SalaryTypeControl: React.FC = () => (
  <div className="options-control">
    {Object.values(SalaryType).map((type) => (
      <label key={type}>
        <Field name="salaryType" component="input" type="radio" value={type} />
        <span className="label">
          {$l.types[type]}
          {type === SalaryType.minWage ? <Info body={$l.help.minWage} /> : null}
        </span>
      </label>
    ))}
  </div>
);

export default SalaryTypeControl;
