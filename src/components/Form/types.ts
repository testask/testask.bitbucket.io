enum SalaryType {
  monthly = 'monthly',
  minWage = 'minWage',
  daily = 'daily',
  hourly = 'hourly',
}

interface Values {
  sum: string,
  salaryType: SalaryType,
  isPure: boolean,
}

export {SalaryType};
export type {Values};
