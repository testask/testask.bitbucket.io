const formatter = new Intl.NumberFormat('ru-RU', {maximumFractionDigits: 0});

const currency = '₽';
const ru = {
  title: 'Сумма',
  currency,
  formatSum: (sum: number): string => `${formatter.format(+sum)} ${currency}`,
  types: {
    monthly: 'Оклад за месяц',
    daily: 'Оклад за день',
    hourly: 'Оклад за час',
    minWage: 'МРОТ',
  },
  taxOptions: [
    'Указать с НДФЛ',
    'Без НДФЛ',
  ],
  help: {
    minWage: 'МРОТ — минимальный размер оплаты труда. Разный для разных регионов.',
    daily: ' в день',
    hourly: ' в час',
  },
  summary: [
    ' сотрудник будет получать на руки',
    ' НДФЛ, 13% от оклада',
    ' за сотрудника в месяц',
  ],
};

export {formatter as numFormatter};
export default ru;
