import React from 'react';

import Form from './components/Form';
import {Values} from './components/Form/types';

const onSubmit = (values: Values) => {
  // eslint-disable-next-line no-alert
  alert(`Submitted values:
${JSON.stringify(values, null, 2)}`);
};

const App: React.FC = () => <Form onSubmit={onSubmit} />;

export default App;
