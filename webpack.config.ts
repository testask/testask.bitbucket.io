import path from 'path';
import webpack from 'webpack';
import CheckTs from 'fork-ts-checker-webpack-plugin';
import ExtractCss from 'mini-css-extract-plugin';
import HtmlWebpack from 'html-webpack-plugin';

const config: webpack.Configuration = {
  entry: ['./src/index.tsx'],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json'],
    alias: {
      lang: path.resolve(__dirname, 'src/lang/'),
      styles: path.resolve(__dirname, 'src/styles/'),
    },
  },
  module: {
    rules: [
      {
        test: /\.[tj]sx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react',
              '@babel/preset-typescript',
            ],
          },
        },
      },
      {
        test: /\.s[ac]ss$/,
        include: [
          path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'src/styles'),
        ],
        use: [
          ExtractCss.loader,
          'css-loader',
          'sass-loader',
        ],
      },
    ],
  },
  plugins: [
    new CheckTs({
      async: false,
      eslint: {files: './src/**/*.{ts,tsx,js,jsx}'},
    }),
    new ExtractCss({
      filename: 'main.[hash:8].min.css',
      chunkFilename: 'chunk.[hash.8].min.css',
    }),
    new HtmlWebpack({
      template: 'src/index.html',
      inlineSource: '.(js|css)$',
      favicon: 'build/favicon.ico',
    }),
  ],
};

export default config;
