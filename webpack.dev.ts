import path from 'path';
import webpack from 'webpack';
import CheckTs from 'fork-ts-checker-webpack-plugin';
import ExtractCss from 'mini-css-extract-plugin';
import HtmlWebpack from 'html-webpack-plugin';

const config: webpack.Configuration = {
  mode: 'development',
  entry: [
    './src/index.tsx',
    'webpack/hot/dev-server',
  ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json'],
    alias: {
      lang: path.resolve(__dirname, 'src/lang/'),
      styles: path.resolve(__dirname, 'src/styles/'),
    },
  },
  module: {
    rules: [
      {
        test: /\.[tj]sx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react',
              '@babel/preset-typescript',
            ],
          },
        },
      },
      {
        test: /\.s[ac]ss$/,
        include: [
          path.join(__dirname, 'node_modules'),
          path.join(__dirname, '/src/styles'),
        ],
        use: [
          ExtractCss.loader,
          'css-loader',
          'sass-loader',
        ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CheckTs({
      async: false,
      eslint: { files: './src/**/*.{ts,tsx,js,jsx}'},
    }),
    new ExtractCss({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    new HtmlWebpack({
      template: 'src/index.html',
      inlineSource: '.(js|css)$',
      favicon: 'build/favicon.ico',
    }),
  ],
  devServer: {
    contentBase: path.join(__dirname, 'build'),
    compress: true,
    port: 3000,
  },
};

export default config;
